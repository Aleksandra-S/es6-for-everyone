const criticalHitChance = () => getRandomInt(2);
const dodgeChance = () => criticalHitChance();
const getRandomInt = (max) => Math.floor(Math.random() * Math.floor(max));

export function fight(firstFighter, secondFighter) {
  const fightOver = () => firstFighter.health <= 0 || secondFighter.health <= 0;

  const punch = (first, second) => (second.health -= getDamage(first, second));

  const nextRound = (first, second) => punch(second, first);

  let winner;

  punch(firstFighter, secondFighter);

  while (!fightOver()) {
    punch(firstFighter, secondFighter);
    if (!fightOver()) {
      nextRound(firstFighter, secondFighter);
    }
  }

  winner = firstFighter.health > 0 ? firstFighter : secondFighter;

  return winner;
}

export function getDamage(attacker, enemy) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);

  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  return fighter.attack * criticalHitChance();
}

export function getBlockPower(fighter) {
  return fighter.defense * dodgeChance();
}
