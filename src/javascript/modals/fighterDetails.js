import { createElement } from "../helpers/domHelper";
import { showModal } from "./modal";

export function showFighterDetailsModal(fighter) {
  const title = "Fighter info";
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({
    tagName: "div",
    className: "modal-body",
  });
  const nameElement = createElement({
    tagName: "span",
    className: "fighter-name",
  });

  // show fighter name, attack, defense, health, image

  const [attackElement, defenseElement, healthElement, imageElement] = [
    createElement({
      tagName: "span",
      className: "fighter-attack",
    }),
    createElement({
      tagName: "span",
      className: "fighter-defense",
    }),
    createElement({
      tagName: "span",
      className: "fighter-health",
    }),
    createElement({
      tagName: "img",
      className: "fighter-image",
    }),
  ];

  nameElement.innerText = name;
  fighterDetails.append(nameElement);

  attackElement.innerHTML = '</br> Damage: ' + attack + '</br>';
  defenseElement.innerHTML = 'Armor: ' + defense + '</br>';
  healthElement.innerHTML = 'Health: ' + health + '</br>';
  imageElement.setAttribute("src", source);

  fighterDetails.append(attackElement, defenseElement, healthElement, imageElement);
  
  return fighterDetails;
}
